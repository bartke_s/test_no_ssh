---
title: "Übungsblatt 6"
author: "Team Foo"
date: "2022-09-07"
output:
  html_document:
    keep_md: true
    toc: yes
    toc_float: yes
    number_sections: yes
  pdf_document:
    toc: yes
---


<!--
Wenn Sie diese oder eine ähnliche Fehlermeldung sehen:
Fehler in library(sozoeko1): es gibt kein Paket namens 'sozoeko1'

Dann klicken Sie unten links auf Console und geben ab dem blinkenden Strich ein:

install.packages("remotes") 
library(remotes)
remotes::install_gitlab("baz9527/sozoeko1", host="gitlab.rrz.uni-hamburg.de")

Versuchen Sie dann, erneut das Rmd zu knitten.
Wenn alles funktioniert, können Sie diesen Kommentar (Zeile 19 bis einschließlich Zeile 31) 
löschen.
-->

